This is a minimal template for using the Rust binding of PyTorch,
[tch](https://lib.rs/crates/tch), with cuda on NixOS.
It's tested to work at least on my machine with an RTX 3070.
